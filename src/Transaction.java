

import java.util.ArrayList;


public class Transaction {

    private int nItem;
    private ArrayList<Rental> list;


    public Transaction() {

        this.list = new ArrayList<Rental>();
        this.nItem = list.size();


    }

    public ArrayList<Rental> getRentalList() {

        return list;
    }

    public void addRental(Rental rental) {

        if (isEmpty()) {
            list = new ArrayList<Rental>();
        }

        list.add(rental);
        nItem++;
    }

    public void removeRental(Movie movie) {

        for (int i = 0; i < list.size(); i++) {

            if (list.get(i).getMovie().equals(movie)) {
                list.remove(i);
            }
        }
        nItem--;

    }

    public int size() {

        return list.size();
    }

    public boolean isEmpty() {

        if (nItem == 0)
            return true;
        return false;
    }

    public int getNItem() {
        return this.nItem;
    }

    //this method will calculate price of movie that customer who has this transaction rent movie
    //it also add every price to Store's income
    public String getTotalPrice() {

        double totalPrice = 0;
        for (int i = 0; i < list.size(); i++) {
            totalPrice += this.list.get(i).getMovie().getPrice();
            Recorder.income=Recorder.income+this.list.get(i).getMovie().getPrice();
        }
        return Double.toString(totalPrice);
    }


}