import java.util.ArrayList;


public class Inventory {

    private int nItem;
    private ArrayList<Movie> list;

    public Inventory() {

        this.list = new ArrayList<Movie>();
        this.nItem = list.size();


    }

    public int getNItem() {
        return this.nItem;
    }

    public boolean isEmpty() {

        if (nItem == 0)
            return true;
        return false;
    }

    public void addMovie(Movie movie) {

        if (isEmpty()) {
            list = new ArrayList<Movie>();
        }

        list.add(movie);
        nItem++;
    }

    //this method will return list of movie that available in movie list that it receive
    public static ArrayList<Movie> findAvailableMovieList(ArrayList<Movie> movieList) {

        ArrayList<Movie> avaList = new ArrayList();

        for (int i = 0; i < movieList.size(); i++) {
            if (movieList.get(i).getAvailable())
                avaList.add(movieList.get(i));

        }
        return avaList;
    }

    //this method will return String
    //content of String is every movie that available in inventory
    public String readyForRent() {

        StringBuffer sb = new StringBuffer("");
        int count=0;

        for (int i = 0; i < list.size(); i++) {

            if (list.get(i).getAvailable() == true) {
                sb.append(count+1+". ");
                sb.append(list.get(i).getName());
                sb.append(" " + "in category of" + " " + list.get(i).getCategory());
                sb.append(" " + "and price is" + " " + list.get(i).getPrice()+"$");
                sb.append("\n");
                count++;
            }
        }
        sb.append("\n");
        sb.append("Number of movie that available is => "+count);
        sb.append("\n");
        return sb.toString();
    }

    public ArrayList<Movie> getList() {
        return this.list;
    }


}