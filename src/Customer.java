import java.util.ArrayList;
import java.util.Date;

public abstract class Customer {

    private String id;
    private String name;
    private Transaction tran;

    public Customer(String id, String name) {

        this.id = id;
        this.name = name;

        this.tran = new Transaction();

    }

    public String getId() {
        return this.id;

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //abstract method for sub class do something difference
    public abstract void willRent(Date toDay, ArrayList<Movie> movieList, int willRent);

    //this method will receive date of current day and check with all date to return of all rental
    //if it match this method will call returnMovie and send movie that want to return

    public void willReturn(Date currDay) {

        ArrayList<Rental> renList = this.tran.getRentalList();
        String currDaystr = currDay.toString();
        for (int i = 0; i < renList.size(); i++) {
            if (renList.get(i).getReturnDate().substring(0, 10).equals(currDaystr.substring(0, 10))) {

                System.out.println(this.getName() + " come to return movie name " + renList.get(i).getMovie().getName());

                this.returnMovie(renList.get(i).getMovie(), currDay);
            }

        }

    }

    public Transaction getTransaction() {
        return this.tran;
    }

    public void setTranasaction(Transaction tran) {
        this.tran = tran;
    }

    //this method receive parameter from willRent (if don't want to random every customer can directly call this method for rent 1 movie)
    //this method will add movie to customer's transaction that keep their rental list and set moive available to false
    //it also add customer's rental to recorder
    protected void rent(Movie movie, Date rentDate, Date returnDate,int numDate) {

        if (this.tran.size() < 3) {
            if (movie.getAvailable()) {

                Rental ren = new Rental(rentDate, returnDate, movie, this.getName(),numDate);
                movie.setAvailable(false);

                this.tran.addRental(ren);
                Recorder.addRental(ren);
                System.out.println(this.getName() + " Congratulation! you rent " + movie.getName() + " Total price is " + this.tran.getTotalPrice()+"$");

            } else {
                System.out.println(this.getName() + "! all movie you choose is unavailable rigth now!");
            }
        } else {
            System.out.println(this.getName() + "! Your rental is now 3 video!");
        }

    }

    //this method receive parameter from willRent (if don't want to random every customer can directly call this method for rent 2 movie)
    //this method will add movie to customer's transaction that keep their rental list and set moive available to false
    //it also add customer's rental to recorder
    //if this customer can't rent movie by some reason it will call rent that has less parameter(overloaded)
    protected void rent(Movie movie1, Movie movie2, Date rentDate, Date returnDate,int numDate) {

        ArrayList<Movie> movieList = new ArrayList();
        movieList.add(movie1);
        movieList.add(movie2);

        if (this.tran.size() <= 1) {
            if (movie1.getAvailable() && movie2.getAvailable()) {

                Rental ren1 = new Rental(rentDate, returnDate, movie1, this.getName(),numDate);
                Rental ren2 = new Rental(rentDate, returnDate, movie2, this.getName(),numDate);
                movie1.setAvailable(false);
                movie2.setAvailable(false);

                this.tran.addRental(ren1);
                this.tran.addRental(ren2);

                Recorder.addRental(ren1);
                Recorder.addRental(ren2);

                System.out.println(this.getName() + " Congratulation! you rent " + movie1.getName() + " and " + movie2.getName() + " Total price is " + this.tran.getTotalPrice()+"$");

            } else {
                System.out.println(this.getName() + "! some movie you choose is unavailable rigth now!");
                ArrayList<Movie> avaList = Inventory.findAvailableMovieList(movieList);
                if (avaList.size() == 1) {
                    this.rent(avaList.get(0), rentDate, returnDate,numDate);
                }
                if (avaList.size() == 0) {
                    System.out.println(this.getName() + "! all movie you choose is unavailable rigth now!");
                }


            }
        } else {
            System.out.println(this.getName() + "! you can't rent more than 3 video");
            ArrayList<Movie> avaList = Inventory.findAvailableMovieList(movieList);
            if (avaList.size() == 1) {
                this.rent(avaList.get(0), rentDate, returnDate,numDate);
            }
            if (avaList.size() == 0) {
                System.out.println(this.getName() + "! all movie you choose is unavailable rigth now!");
            }
        }

    }

    //this method receive parameter from willRent (if don't want to random every customer can directly call this method for rent 3 movie)
    //this method will add movie to customer's transaction that keep their rental list and set moive available to false
    //it also add customer's rental to recorder
    //if this customer can't rent movie by some reason it will call rent that has less parameter(overloaded)

    protected void rent(Movie movie1, Movie movie2, Movie movie3, Date rentDate, Date returnDate,int numDate) {

        ArrayList<Movie> movieList = new ArrayList();
        movieList.add(movie1);
        movieList.add(movie2);
        movieList.add(movie3);

        if (this.tran.size() == 0) {
            if (movie1.getAvailable() && movie2.getAvailable() && movie3.getAvailable()) {


                Rental ren1 = new Rental(rentDate, returnDate, movie1, this.getName(),numDate);
                Rental ren2 = new Rental(rentDate, returnDate, movie2, this.getName(),numDate);
                Rental ren3 = new Rental(rentDate, returnDate, movie3, this.getName(),numDate);

                movie1.setAvailable(false);
                movie2.setAvailable(false);
                movie3.setAvailable(false);

                this.tran.addRental(ren1);
                this.tran.addRental(ren2);
                this.tran.addRental(ren3);

                Recorder.addRental(ren1);
                Recorder.addRental(ren2);
                Recorder.addRental(ren3);

                System.out.println(this.getName() + " Congratulation! you rent " + movie1.getName() + "," + movie2.getName() + " and " + movie3.getName() + " Total price is " + this.tran.getTotalPrice()+"$");

            } else {
                ArrayList<Movie> avaList = Inventory.findAvailableMovieList(movieList);
                System.out.println(this.getName() + "! some movie you choose is unavailable rigth now!");
                if (avaList.size() == 2) {
                    this.rent(avaList.get(0), avaList.get(1), rentDate, returnDate,numDate);
                }
                if (avaList.size() == 1) {
                    this.rent(avaList.get(0), rentDate, returnDate,numDate);
                }
                if (avaList.size() == 0) {
                    System.out.println(this.getName() + "! all movie you choose is unavailable rigth now!");
                }
            }
        } else {
            System.out.println(this.getName() + "! you can't rent more than 3 video");
            ArrayList<Movie> avaList = Inventory.findAvailableMovieList(movieList);
            if (avaList.size() == 3 || avaList.size() == 2) {
                this.rent(avaList.get(0), avaList.get(1), rentDate, returnDate,numDate);
            }
            if (avaList.size() == 1) {
                this.rent(avaList.get(0), rentDate, returnDate,numDate);
            }
            if (avaList.size() == 0) {
                System.out.println(this.getName() + "! all movie you choose is unavailable rigth now!");
            }


        }

    }

    //this method receive movie from willReturn method and remove 1 rental of this customer and set Available to movie
    protected void returnMovie(Movie movie, Date currDay) {

        this.tran.removeRental(movie);
        movie.setAvailable(true);
        System.out.println(">>>>> " + movie.getName() + " movie has been return! <<<<<<");
        this.willReturn(currDay);

    }

    //this method print every rental that this customer still not return
    public void history() {

        if (!tran.isEmpty()) {

            for (int i = 0; i < tran.size(); i++) {
                System.out.print((i+1)+". ");
                System.out.println(this.getName() + " still not return " + tran.getRentalList().get(i).getMovie().getName()+" Price : "+tran.getRentalList().get(i).getMovie().getPrice()+"$ for "+tran.getRentalList().get(i).getNumDate()+" night \n"+" that rent on "+tran.getRentalList().get(i).getRentDate()+" and has to return on "+tran.getRentalList().get(i).getReturnDate());
            }
        }
    }

}