import java.util.Date;

public class Rental {

    private Date rentDate;
    private Date returnDate;
    private Movie movie;
    private String cName;
    private int numDate;



    public Rental(Date rentDate,Date returnDate,Movie movie,String cName,int numDate) {

        this.rentDate = rentDate;
        this.returnDate = returnDate;
        this.movie = movie;
        this.cName = cName;
        this.numDate = numDate;


    }

    public Movie getMovie() {

        return this.movie;

    }

    public String getcName(){

        return this.cName;

    }

    public int getNumDate(){
        return this.numDate;
    }

    public String getRentDate(){

        return this.rentDate.toString();

    }

    public String getReturnDate(){

        return this.returnDate.toString();

    }


}
