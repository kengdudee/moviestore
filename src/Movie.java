
public class Movie {

    private String category;
    private double price;
    private String name;
    private boolean isAvailable;

    public Movie(Enum category, String name) {

        this.category = category.toString();
        this.name = name;
        this.isAvailable = true;

        if(category.compareTo(Category.COMEDY)==0){
            this.price = 1;
        }

        if(category.compareTo(Category.ROMANCE)==0){
            this.price = 2;
        }

        if(category.compareTo(Category.DRAMA)==0){
            this.price = 2;
        }
        if(category.compareTo(Category.NEWRELEASE)==0){
            this.price = 5;
        }
        if(category.compareTo(Category.HORROR)==0){
            this.price = 3;
        }

    }

    public enum Category {
        NEWRELEASE,DRAMA,COMEDY,ROMANCE,HORROR;

        @Override
        public String toString() {
            switch(this) {

                case NEWRELEASE: return "New Release";
                case DRAMA: return "Drama";
                case COMEDY: return "Comedy";
                case ROMANCE: return "Romance";
                case HORROR: return "Horror";

                default: throw new IllegalArgumentException();
            }
        }
    }

    public String getCategory() {
        return this.category;
    }

    public double getPrice() {
        return this.price;
    }

    public String getName() {
        return this.name;
    }

    public boolean getAvailable() {
        return this.isAvailable;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }


}