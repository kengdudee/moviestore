import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class BreezyCustomer extends Customer {

    public BreezyCustomer(String id, String name) {

        super(id, name);


    }
    //this method will call rent method and send movie , date of today , date of return day , number of night to return
    public void willRent(Date toDay,ArrayList<Movie> movieList, int willRent) {

        if (willRent == 1) {
            Random rand = new Random();
            int numMovie = rand.nextInt(3);
            //numMovie+=1;

            if (numMovie == 1) {
                int randomMovie = rand.nextInt(movieList.size());
                //randomMovie+=1;


                int rann = rand.nextInt(2);
                rann++;
                Date currentDate = toDay;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);
                calendar.add(Calendar.DAY_OF_YEAR, rann);

                Date return12Day = calendar.getTime();
                System.out.println(this.getName() + " will rent " + movieList.get(randomMovie).getName()+" for "+rann+" night");

                this.rent(movieList.get(randomMovie), toDay,return12Day,rann);

            }
            if (numMovie == 2) {
                int random1 = rand.nextInt(movieList.size());
                //random1+=1;
                int random2;
                while (true) {
                    random2 = rand.nextInt(movieList.size());
                    //random2+=1;
                    if (random2 != random1)
                        break;
                }


                int rann = rand.nextInt(2);
                rann++;
                Date currentDate = toDay;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);
                calendar.add(Calendar.DAY_OF_YEAR, rann);

                Date return12Day = calendar.getTime();

                System.out.println(this.getName() + " will rent " + movieList.get(random1).getName() + " and " + movieList.get(random2).getName()+" for "+rann+" night");
                this.rent(movieList.get(random1), movieList.get(random2), toDay,return12Day,rann);

            }


        }
    }


}
