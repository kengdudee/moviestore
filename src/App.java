import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class App {
    public static void main(String[] args) throws Exception {

        //create customer
        //both parameter is String type
        HoardersCustomer keng = new HoardersCustomer("1", "Supakitpon");
        BreezyCustomer psila = new BreezyCustomer("2", "Sila");
        RegularCustomer third = new RegularCustomer("3", "Third");
        BreezyCustomer pdune = new BreezyCustomer("4", "Raknatee");
        BreezyCustomer f = new BreezyCustomer("5", "Pongsak");
        BreezyCustomer s = new BreezyCustomer("6", "Natta");
        BreezyCustomer sa = new BreezyCustomer("7", "Krit");
        BreezyCustomer e = new BreezyCustomer("8", "Joe");
        RegularCustomer n = new RegularCustomer("9", "Jane");
        HoardersCustomer t = new HoardersCustomer("10", "Meaw");

        //create list of customer
        CustomerList cList = new CustomerList();

        //add each customer to their list
        cList.addHCustomer(keng);
        cList.addBCustomer(psila);
        cList.addRCustomer(third);
        cList.addBCustomer(pdune);
        cList.addBCustomer(f);
        cList.addBCustomer(s);
        cList.addBCustomer(sa);
        cList.addBCustomer(e);
        cList.addRCustomer(n);
        cList.addHCustomer(t);



        //create movie
        //first parameter is enum type and second is String type
        Movie a = new Movie(Movie.Category.NEWRELEASE, "ALITA");
        Movie b = new Movie(Movie.Category.NEWRELEASE, "How to train your dragon3");
        Movie c = new Movie(Movie.Category.NEWRELEASE, "Replicas");
        Movie d = new Movie(Movie.Category.NEWRELEASE, "Spider-Man into the Spider-verse");


        Movie ee = new Movie(Movie.Category.DRAMA, "Sky hunter");
        Movie ff = new Movie(Movie.Category.DRAMA, "The Hate U Give");
        Movie g = new Movie(Movie.Category.DRAMA, "bohemian Rhapsody");
        Movie h = new Movie(Movie.Category.DRAMA, "Escape Room");


        Movie ii = new Movie(Movie.Category.COMEDY, "The Grinch");
        Movie jj = new Movie(Movie.Category.COMEDY, "Bikeman");
        Movie k = new Movie(Movie.Category.COMEDY, "Igor");
        Movie l = new Movie(Movie.Category.COMEDY, "Megamind");

        Movie m = new Movie(Movie.Category.ROMANCE, "50 First kisses");
        Movie nn = new Movie(Movie.Category.ROMANCE, "I Love You Beth Cooper");
        Movie o = new Movie(Movie.Category.ROMANCE, "Be with you");
        Movie p = new Movie(Movie.Category.ROMANCE, "La La Land");


        Movie q = new Movie(Movie.Category.HORROR, "The Prodigy");
        Movie r = new Movie(Movie.Category.HORROR, "The cave");
        Movie ss = new Movie(Movie.Category.HORROR, "Dead sea");
        Movie tt = new Movie(Movie.Category.HORROR, "Visions");

        //create Inventory that keep movie
        Inventory inven = new Inventory();

        //add movie to Inventory
        inven.addMovie(a);
        inven.addMovie(b);
        inven.addMovie(c);
        inven.addMovie(d);

        inven.addMovie(ee);
        inven.addMovie(ff);
        inven.addMovie(g);
        inven.addMovie(h);

        inven.addMovie(ii);
        inven.addMovie(jj);
        inven.addMovie(k);
        inven.addMovie(l);

        inven.addMovie(m);
        inven.addMovie(nn);
        inven.addMovie(o);
        inven.addMovie(p);

        inven.addMovie(q);
        inven.addMovie(r);
        inven.addMovie(ss);
        inven.addMovie(tt);

        //create new reference of array list of Movie class and initial with list of movie in Inventory
        ArrayList<Movie> movieList = inven.getList();

        //create Calendar
        Calendar calendar = Calendar.getInstance();

        //create Array of String that will contain String of date
        String[] day = new String[35];

        System.out.println("_____________________Start Simulation_________________________"+"\n");


        //each loop is mean each day
        for (int i = 0; i < 35; i++) {
            System.out.println("\n"+"Day "+(i+1));
            if (i != 0) {
                //plus day by 1 and it should not do in first day
                calendar.add(Calendar.DAY_OF_YEAR, 1);
            }

            //create day
            Date toDay = calendar.getTime();

            //convert day(Class Date) to String
            String str = toDay.toString();

            //use day[i] to print each day
            day[i] = str;
            System.out.println("____________" + day[i] + "______________");


            if (inven.readyForRent().length() != 0) {
                System.out.println(" ");
                System.out.println("___________Available movie in store at start of the day____________\n" + inven.readyForRent());
                System.out.println("_____________________________________________________________________");
            } else {
                System.out.println(" ");
                System.out.println("___________Available movie in store at start of the day____________\n" + "out of stock!");
                System.out.println("_____________________________________________________________________");
            }
            //each customer will do this every day for check that they has to return movie or not
            //if reach return day willReturn method will call returnMovie method for return movie
            keng.willReturn(toDay);
            psila.willReturn(toDay);
            third.willReturn(toDay);
            pdune.willReturn(toDay);
            f.willReturn(toDay);
            s.willReturn(toDay);
            sa.willReturn(toDay);
            e.willReturn(toDay);
            n.willReturn(toDay);
            t.willReturn(toDay);

            if (inven.readyForRent().length() != 0) {
                System.out.println(" ");
                System.out.println("___________Available movie after every customer return their movie in store in this day____________\n" + inven.readyForRent());
                System.out.println("_____________________________________________________________________");
            } else {
                System.out.println(" ");
                System.out.println("___________Available movie after every customer return their movie in store in this day____________\n" + "out of stock!");
                System.out.println("_____________________________________________________________________");
            }

            //create random for random customer to come in the store
            Random rand = new Random();
            int randomComeIn = rand.nextInt(3);
            randomComeIn++;

            //do this for get customer know that no movie available in this store so they won't come until some one rent movie
            ArrayList<Movie> avaList = Inventory.findAvailableMovieList(movieList);
            if(avaList.size()==0){
                randomComeIn=0;
            }

            System.out.println("\n"+"______________________________Action Begin____________________________________\n");


            //this condition for HoardersCustomer will come to rent
            //for willRent method in it's third parameter is customer's decision that they will rent this day or not
            //and willRent will call rent method for rent movie
            if (randomComeIn == 1&&avaList.size()>=3) {


                keng.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));
                t.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));

            }
            else{
                System.out.println("no Hoarders Customer come to day...");
            }

            //this condition for RegularCustomer will come to rent
            if (randomComeIn == 2) {


                third.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));
                n.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));


            }
            else{
                System.out.println("no Regular Customer come to day...");
            }

            //this condition for BreezyCustomer will come to rent
            if (randomComeIn == 3) {


                psila.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));
                pdune.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));
                f.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));
                s.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));
                sa.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));
                e.willRent(toDay, movieList, (int) (Math.random() * 2 + 1));

            }
            else{
                System.out.println("no Breezy Customer come to day...");
            }


            //print all available movie name in inventory every day and all number of them
            if (inven.readyForRent().length() != 0) {
                System.out.println(" ");
                System.out.println("___________Available movie in store at the end of the day____________\n" + inven.readyForRent());
                System.out.println("_____________________________________________________________________");
            } else {
                System.out.println(" ");
                System.out.println("___________Available movie in store at the end of the day____________\n" + "out of stock!");
                System.out.println("_____________________________________________________________________");
            }

        }


        System.out.println("_____________________End Simulation_________________________"+"\n");

        System.out.println("Result :"+"\n");

        System.out.println("Number of videos currently in store is along with a list of their names is \n"+inven.readyForRent()+"\n");

        //print Store's income for the whole time
        System.out.println("Amount of money the store made during the 35 days is "+ Recorder.income+"$"+"\n");
        //this will print history of all returned movie

        System.out.println("A list of all the completed rentals is \n");
        Recorder.history(cList.getBItem() + cList.getHItem() + cList.getRItem());
        System.out.println(" ");

        System.out.println("A list of all active rentals (group by type of customer) is \n");

        //this will print history of all movie that not return yet
        System.out.println("_______Breezy Customer______"+"\n");

        for (int j = 0; j < cList.getBItem(); j++) {

            cList.getBlist().get(j).history();
        }
        System.out.println(" ");
        System.out.println("_______Hoarders Customer______"+"\n");


        for (int j = 0; j < cList.getHItem(); j++) {

            cList.getHlist().get(j).history();
        }
        System.out.println(" ");
        System.out.println("_______Regular Customer______"+"\n");

        for (int j = 0; j < cList.getRItem(); j++) {

            cList.getRlist().get(j).history();
        }
        System.out.println("___________________________________");

        System.out.println(" ");



    }

}

