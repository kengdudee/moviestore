import java.util.ArrayList;

public class Recorder {

    public static double income;
    private static int nItem;
    private static ArrayList<Rental> list;

    public Recorder() {
        income = 0;
    }

    public static int size() {

        return list.size();
    }

    public static boolean isEmpty() {

        if (nItem == 0)
            return true;
        return false;
    }

    public static int getNItem() {
        return nItem;
    }

    public static void addRental(Rental rental) {

        if (isEmpty()) {
            list = new ArrayList<Rental>();
        }

        list.add(rental);
        nItem++;
    }

    //this method will print all record movie
    public static void history(int stop) {



        for (int i = 0; i < list.size()-stop; i++) {
                System.out.print((i+1)+". ");
                System.out.println(list.get(i).getcName() + " rent " + list.get(i).getMovie().getName() + " Price : " + list.get(i).getMovie().getPrice() + "$ for "+list.get(i).getNumDate()+" night\n on " + list.get(i).getRentDate() + " and returned on " + list.get(i).getReturnDate());


        }


    }


}
