import java.util.ArrayList;

public class CustomerList {

    private int bItem;
    private int hItem;
    private int rItem;
    private ArrayList<BreezyCustomer> blist;
    private ArrayList<HoardersCustomer> hlist;
    private ArrayList<RegularCustomer> rlist;

    public CustomerList() {

        this.blist = new ArrayList<BreezyCustomer>();
        this.bItem = blist.size();

        this.hlist = new ArrayList<HoardersCustomer>();
        this.hItem = hlist.size();

        this.rlist = new ArrayList<RegularCustomer>();
        this.rItem = rlist.size();

    }

    public int getBItem() {
        return this.bItem;
    }

    public int getHItem() {
        return this.hItem;
    }

    public int getRItem() {
        return this.rItem;
    }

    public ArrayList<BreezyCustomer> getBlist(){
        return blist;
    }
    public ArrayList<HoardersCustomer> getHlist(){
        return hlist;
    }
    public ArrayList<RegularCustomer> getRlist(){
        return rlist;
    }

    public boolean isbEmpty() {

        if (bItem == 0)
            return true;
        return false;
    }

    public boolean ishEmpty() {

        if (hItem == 0)
            return true;
        return false;
    }

    public boolean isrEmpty() {

        if (rItem == 0)
            return true;
        return false;
    }

    public void addBCustomer(BreezyCustomer customer) {

        if (isbEmpty()) {
            blist = new ArrayList<BreezyCustomer>();
        }

        blist.add(customer);
        bItem++;

    }

    public void addHCustomer(HoardersCustomer customer) {

        if (ishEmpty()) {
            hlist = new ArrayList<HoardersCustomer>();
        }

        hlist.add(customer);
        hItem++;

    }

    public void addRCustomer(RegularCustomer customer) {

        if (isrEmpty()) {
            rlist = new ArrayList<RegularCustomer>();
        }

        rlist.add(customer);
        rItem++;

    }


}
