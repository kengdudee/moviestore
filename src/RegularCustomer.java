import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class RegularCustomer extends Customer {

    public RegularCustomer(String id, String name) {

        super(id, name);

    }
    //this method will call rent method and send movie , date of today , date of return day , number of night to return
    public void willRent(Date toDay,ArrayList<Movie> movieList, int willRent) {

        if (willRent == 1) {

            Random rand = new Random();
            int numMovie = rand.nextInt(4);



            if (numMovie == 1) {
                int randomMovie = rand.nextInt(movieList.size());


                int rann = rand.nextInt(3);
                rann += 3;
                Date currentDate = toDay;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);
                calendar.add(Calendar.DAY_OF_YEAR, rann);

                Date return35Day = calendar.getTime();
                System.out.println(this.getName() + " will rent " + movieList.get(randomMovie).getName()+" for "+rann+" night");


                this.rent(movieList.get(randomMovie), toDay,return35Day,rann);

            }
            if (numMovie == 2) {
                int random1 = rand.nextInt(movieList.size());

                int random2;
                while (true) {
                    random2 = rand.nextInt(movieList.size());

                    if (random2 != random1)
                        break;
                }

                int rann = rand.nextInt(3);
                rann += 3;
                Date currentDate = toDay;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);
                calendar.add(Calendar.DAY_OF_YEAR, rann);

                Date return35Day = calendar.getTime();
                System.out.println(this.getName() + " will rent " + movieList.get(random1).getName() + " and " + movieList.get(random2).getName()+" for "+rann+" night");

                this.rent(movieList.get(random1), movieList.get(random2), toDay,return35Day,rann);

            }
            if (numMovie == 3) {

                int random1 = rand.nextInt(movieList.size());

                int random2;
                int random3;
                while (true) {
                    random2 = rand.nextInt(movieList.size());

                    if (random2 != random1)
                        break;
                }
                while (true) {
                    random3 = rand.nextInt(movieList.size());

                    if (random3 != random1 && random3 != random2) {
                        break;
                    }

                }

                int rann = rand.nextInt(3);
                rann += 3;
                Date currentDate = toDay;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currentDate);
                calendar.add(Calendar.DAY_OF_YEAR, rann);

                Date return35Day = calendar.getTime();

                System.out.println(this.getName() + " will rent " + movieList.get(random1).getName() + " , " + movieList.get(random2).getName() + " and " + movieList.get(random3).getName()+" for "+rann+" night");

                this.rent(movieList.get(random1), movieList.get(random2), movieList.get(random3), toDay,return35Day,rann);

            }
        }
    }


}
